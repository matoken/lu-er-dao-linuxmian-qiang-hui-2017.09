# 鹿児島Linux勉強会 2017.09

## 開催日?

16日 -> 台風のため18日に延期


          9月 2017         
    日 月 火 水 木 金 土  
                    1  2  
     3  4  5  6  7  8  9  
    10 11 12 13 14 15 16  
    17 祝 19 20 21 22 祝  
    24 25 26 27 28 29 30  

祝日

* 18(mon) : 敬老の日
* 23(sat) : 秋分の日

イベント

* 02 : OSC千葉
* 07-09 : PyConJP2017
* 09-10 : OSC東京
* 15-16 : FOSS4G
* 16 : SFD / 小江戸らぐのオフな集まり / 東海道らぐのみかい@名古屋


## 会場

☒TUKUDDOペラ位置フォームから問い合わせ
☒OKの回答あり

> 
会場 モノづくりスペース TUKUDDO
住所 鹿児島県鹿児島市泉町１４−１２(奥山ビル1F)
http://tukuddo.com/


## 申し込みページ

[鹿児島Linux勉強会 2017.09 - connpass](https://kagolug.connpass.com/event/66361/ "鹿児島Linux勉強会 2017.09 - connpass")


## アナウンス

☒connpass https://kagolug.connpass.com/
☒site修正 https://kagolug.org/
☒gnusocial告知修正 https://gnusocial.kagolug.org/
☒ML告知
☒twitter告知 @kagolug
☒gnusocial告知 @info
☒G+告知 +KenichiroMATOHARA
☒SFD Wiki登録 https://www.softwarefreedomday.org/


## 申込者人数通知

```
8 */6 * * *     date +"#kagolug "\%Y-\%m-\%d\ \%H:\%M頃の申込者数は`curl -s 'https://connpass.com/api/v1/event/?event_id=66361' | ~/bin/jq '.events|.[0].accepted'`人です | DISPLAY=:0 xargs -I{} notify-send -t 0 {}
```


## @matoken ネタ

* Wikiネタ
* lawspec machineネタ
* BlueBorne ネタ
* cli speedtestネタ

