= Wikiシステムを試す

== 軽量マークアップ言語が使えてDBとか要らない感じのWIkiが欲しい

- MarkDown/AsciiDocあたりが使えると嬉しい
- 個人で使うだけなのでデータ量も少ないしDBとか面倒
- 管理が面倒なのでできたらaptで入るもの

== gititとgollum

== gitit

- pandocがバックエンドでmarkdown, rst, latex, html, markdown+lhs, rst+lhs, latex+lhsが利用できる
- dbは必要なくてgitで管理
- aptで入る
- ユーザ管理もwebでok


== gollum

- Creole, MarkDown, MediaWiki, Org-mode, Plain Text, Pod, RDoc, ReStracturedText, Textileが利用できる
-- asciidoctorを入れるとAsciiDocも利用できるようになる
- ウェブで編集すると全部同じ人になってしまう
-- 複数人で編集する場合はgitで管理するしかなさそう


== gitit導入

.install
----
$ sudo apt install gitit
----

- gititコマンドを実行するだけで環境を作ってくれる．
- http://localhost:5001 でhttpdサーバーも起動する．

== gitit設定

.設定変更には設定ファイルを作成して編集する．
----
$ gitit --print-default-config > gitit.conf
----

.設定ファイルはgititに`-f`オプションで指定する．
----
$ cd gitit-dir
$ gitit -f gitit.conf
----


== gollum導入

----
$ gem install gollum --user-install
$ mkdir gollum_wiki
$ cd gollum_wiki
$ git init
$ find ~/ -name gollum
$ find ~/ -name gollum -type f
/home/mk/.gem/ruby/2.3.0/gems/gollum-4.1.2/bin/gollum
/home/mk/.gem/ruby/2.3.0/gems/gollum-4.1.2/contrib/openrc/conf.d/gollum
/home/mk/.gem/ruby/2.3.0/gems/gollum-4.1.2/contrib/openrc/init.d/gollum
/home/mk/.gem/ruby/2.3.0/gems/gollum-4.1.2/contrib/sysv-debian/init.d/gollum
/home/mk/.gem/ruby/2.3.0/bin/gollum
~/gem/ruby/2.3.0/bin/gollum &
----

http://localhost:4567/ でアクセスできる


== gollum認証

- 一人で使うので認証(BASIC AUTH……)を付けたい

.auth.rb 
----
module Precious
  class App < Sinatra::Base
    use Rack::Auth::Basic, "Restricted Area" do |username, password|
      [username, password] == ["user", "password"]
    end
  end
----

.auth.rbを付けて起動
----
$ gollum --config auth.rb
----

※OmniAuthでGitHUB認証の例 -> http://qiita.com/shohei1913/items/9498fbd9d9e2c5b97c2c[wikiエンジン"gollum"をEC2上にインストールしてカスタマイズするまで - Qiita]


== apache httpdの設定例(subdomain)

.http://wiki.example.jp/ としてアクセスする時の例
----
<VirtualHost *:80>
        ServerName wiki.example.jp
        ServerAdmin wiki-admin@example.jp

        RewriteEngine On
        ProxyPreserveHost On
        ProxyRequests Off

        <Proxy *>
                Order deny,allow
                Allow from all
        </Proxy>

        ProxyPassReverse /    http://127.0.0.1:5001
        RewriteRule ^(.*) http://127.0.0.1:5001$1 [P]

        ErrorLog ${APACHE_LOG_DIR}/error-gitit.log

        # Possible values include: debug, info, notice, warn, error, crit,
        # alert, emerg.
        LogLevel warn

        CustomLog ${APACHE_LOG_DIR}/access-gitit.log combined

        ServerSignature On

</VirtualHost>
----

== apache httpdの設定例(path)

.http://example.jp/wiki/ としてアクセスする時の例
----
        ProxyPreserveHost On
        ProxyPass "/wiki" "http://localhost:4567/wiki" retry=0
        ProxyPassReverse "/wiki" "http://localhost:4567/wiki"
----

.golloumに--base-path optionを付与
----
$ gollum --base-path wiki
----

== 今回はgollum

- 複数人でwebを併用する場合はgititが良さそう
- 今回は一人で運用だしAsciiDocが使えるしgititは別のところで運用しているのでとりあえずgollumを試している

